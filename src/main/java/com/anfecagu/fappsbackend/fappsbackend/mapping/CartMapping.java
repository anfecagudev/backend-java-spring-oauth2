package com.anfecagu.fappsbackend.fappsbackend.mapping;

import com.anfecagu.fappsbackend.fappsbackend.domain.Cart;
import com.anfecagu.fappsbackend.fappsbackend.persistence.CartDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CartMapping {
    CartDTO CartToDTO (final Cart cart);
    Cart dtoToCart(final CartDTO cartDTO);
}