package com.anfecagu.fappsbackend.fappsbackend.mapping;

import com.anfecagu.fappsbackend.fappsbackend.domain.ProductItem;
import com.anfecagu.fappsbackend.fappsbackend.persistence.ProductItemDTO;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface ProductItemMapping {
    ProductItemDTO productItemToDTO (final ProductItem productItem);
    ProductItem dtoToProductItem(final ProductItemDTO productItemDTO);
}



