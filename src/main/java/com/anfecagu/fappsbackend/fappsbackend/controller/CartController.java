package com.anfecagu.fappsbackend.fappsbackend.controller;


import com.anfecagu.fappsbackend.fappsbackend.domain.Cart;
import com.anfecagu.fappsbackend.fappsbackend.domain.CartItem;
import com.anfecagu.fappsbackend.fappsbackend.domain.ProductItem;
import com.anfecagu.fappsbackend.fappsbackend.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * Controller Class for Cart
 * @author: andres cardona
 */
@RestController
@RequestMapping("/cart")
public class CartController {

    public CartController(@Autowired final CartService cartService){
        this.cartService = cartService;
    }

    private CartService cartService;

    @PostMapping("/savecart")
    public Mono<Cart> saveCart(@RequestBody final List<CartItem> items){
        return cartService.saveCart(items);
    }

    @GetMapping("/cart")
    public Mono<Cart> getCart(){
        return cartService.getCart();
    }
}
