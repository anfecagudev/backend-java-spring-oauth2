package com.anfecagu.fappsbackend.fappsbackend.controller;

import com.anfecagu.fappsbackend.fappsbackend.domain.User;
import com.anfecagu.fappsbackend.fappsbackend.domain.UserPrincipal;
import com.anfecagu.fappsbackend.fappsbackend.persistence.repository.UserRepository;
import com.anfecagu.fappsbackend.fappsbackend.security.CurrentUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * Not used
 * Controller to get info about current user
 */
@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/user/me")
    @PreAuthorize("hasRole('USER')")
    public Mono<User>getCurrentUser(@CurrentUser UserPrincipal userPrincipal) {
        return userRepository.findById(userPrincipal.getId()
//                .onErrorReturn(User.builder()
//                        .name("undefined")
//                        .id(userPrincipal.getId())
//                        .build()
                );
    }

}
