package com.anfecagu.fappsbackend.fappsbackend.controller;


import com.anfecagu.fappsbackend.fappsbackend.domain.User;
import com.anfecagu.fappsbackend.fappsbackend.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/saveuser")
public class SaveUserController {

    @Autowired
    private UserRepository userRepository;

    @PostMapping
    public Mono<User> saveuser(@RequestBody User user){
        return userRepository.save(user);
    }
}
