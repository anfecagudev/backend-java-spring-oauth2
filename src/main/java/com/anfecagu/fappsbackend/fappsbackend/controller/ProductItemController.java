package com.anfecagu.fappsbackend.fappsbackend.controller;

import com.anfecagu.fappsbackend.fappsbackend.domain.ProductItem;
import com.anfecagu.fappsbackend.fappsbackend.service.ProductItemService;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import reactor.core.publisher.Mono;

import java.security.Principal;

/**
 * Controller class for products
 * @author Andres Cardona
 */
@RestController
@RequestMapping("/users")
public class ProductItemController {

    @Autowired
    private ProductItemService productItemService;

    @GetMapping("/token")
    public Mono<Principal> user(Principal principal){
        return Mono.just(principal);
    }

    @GetMapping("status/check")
    public Mono<String> status(){
        return Mono.just("Working...");
    }


    @GetMapping(value = "datalist")
    public Publisher<ProductItem> getProductItems(){
        return productItemService.getProductItems();
    }

    @PostMapping("saveproduct")
    public Mono<ProductItem> saveProductItem(@RequestBody ProductItem productItem){
        return productItemService.saveProductItem(productItem);
    }

}


