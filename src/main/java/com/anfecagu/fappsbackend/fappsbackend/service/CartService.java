package com.anfecagu.fappsbackend.fappsbackend.service;

import com.anfecagu.fappsbackend.fappsbackend.domain.Cart;
import com.anfecagu.fappsbackend.fappsbackend.domain.CartItem;
import com.anfecagu.fappsbackend.fappsbackend.domain.ProductItem;
import reactor.core.publisher.Mono;

import java.util.List;

public interface CartService {

   Mono<Cart> saveCart(List<CartItem> productItems);

   Mono<Cart> createCart(final Long userId, final List<CartItem> productItems);

   Mono<Cart> getCart();
}
