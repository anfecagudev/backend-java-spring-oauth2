package com.anfecagu.fappsbackend.fappsbackend.service.impl;

import com.anfecagu.fappsbackend.fappsbackend.domain.Cart;
import com.anfecagu.fappsbackend.fappsbackend.domain.CartItem;
import com.anfecagu.fappsbackend.fappsbackend.domain.ProductItem;
import com.anfecagu.fappsbackend.fappsbackend.mapping.CartMapping;
import com.anfecagu.fappsbackend.fappsbackend.persistence.CartDTO;
import com.anfecagu.fappsbackend.fappsbackend.persistence.repository.CartRepository;
import com.anfecagu.fappsbackend.fappsbackend.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

/***
 * Service class to manage Product Carts
 * @author Andres Cardona
 */
@Service
public class CartServiceImpl implements CartService {

    @Autowired
    public CartServiceImpl(final TokenProvider tokenProvider,
                           final CartRepository cartRepository,
                           final CartMapping cartMapping) {
        this.tokenProvider = tokenProvider;
        this.cartRepository = cartRepository;
        this.cartMapping = cartMapping;
    }

    private TokenProvider tokenProvider;

    private CartRepository cartRepository;

    private CartMapping cartMapping;

    @Override
    public Mono<Cart> saveCart(final List<CartItem> cartItems) {
        Long userId = tokenProvider.getUserIdFromContext();
        Mono<CartDTO> cartDTO = cartRepository.findByUserId(userId);
        return cartDTO.flatMap(cartDTO1 -> {
            cartDTO1.setProducts(cartItems);
            return cartRepository.save(cartDTO1)
                    .map(cartMapping::dtoToCart);
        }).switchIfEmpty(createCart(userId, cartItems));
    }

    @Override
    public Mono<Cart> createCart(final Long userId, final List<CartItem> cartItems) {
        CartDTO cartDTO = CartDTO.builder()
                .products(cartItems)
                .userId(userId)
                .build();
        return cartRepository.save(cartDTO)
                .map(cartMapping::dtoToCart);
    }

    @Override
    public Mono<Cart> getCart() {
        Long userId = tokenProvider.getUserIdFromContext();
        return cartRepository.findByUserId(userId)
                .map(cartMapping::dtoToCart)
                .switchIfEmpty(createCart(userId, new ArrayList<>()));
    }
}

