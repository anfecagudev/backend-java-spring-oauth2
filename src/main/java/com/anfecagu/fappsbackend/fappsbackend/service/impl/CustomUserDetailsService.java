package com.anfecagu.fappsbackend.fappsbackend.service.impl;

import com.anfecagu.fappsbackend.fappsbackend.domain.User;
import com.anfecagu.fappsbackend.fappsbackend.domain.UserPrincipal;
import com.anfecagu.fappsbackend.fappsbackend.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Custom Service Class for user Details
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {


    @Autowired
    public CustomUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email).block();
        return UserPrincipal.create(user);
    }

    @Transactional
    public UserDetails loadUserById(Long id) {
        User user = userRepository.findById(id).block();
        return UserPrincipal.create(user);
    }
}
