package com.anfecagu.fappsbackend.fappsbackend.service.impl;

import com.anfecagu.fappsbackend.fappsbackend.domain.ProductItem;
import com.anfecagu.fappsbackend.fappsbackend.mapping.ProductItemMapping;
import com.anfecagu.fappsbackend.fappsbackend.persistence.ProductItemDTO;
import com.anfecagu.fappsbackend.fappsbackend.persistence.repository.ProductItemRepository;
import com.anfecagu.fappsbackend.fappsbackend.service.ProductItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service Class for ProducItem Handling
 * @author andres cardona
 */
@Service
public class ProductItemServiceImpl implements ProductItemService {

    @Autowired
    public ProductItemServiceImpl(ProductItemRepository productItemRepository, ProductItemMapping productItemMapping) {
        this.productItemRepository = productItemRepository;
        this.productItemMapping = productItemMapping;
    }

    private ProductItemRepository productItemRepository;


    private ProductItemMapping productItemMapping;

    @Override
    public Flux<ProductItem> getProductItems() {
        Flux<ProductItemDTO> productItemDTOs = productItemRepository.findAll();
        return productItemDTOs
                .map(productItemMapping::dtoToProductItem).log();
    }

    @Override
    public Mono<ProductItem> saveProductItem(ProductItem productItem) {
        ProductItemDTO itemToSave = productItemMapping.productItemToDTO(productItem);
        Mono<ProductItemDTO> itemSaved = productItemRepository.save(itemToSave);
        return itemSaved
                .map(productItemMapping::dtoToProductItem).log();
    }
}
