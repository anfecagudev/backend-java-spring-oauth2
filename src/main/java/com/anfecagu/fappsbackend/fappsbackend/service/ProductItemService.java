package com.anfecagu.fappsbackend.fappsbackend.service;

import com.anfecagu.fappsbackend.fappsbackend.domain.ProductItem;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ProductItemService {

    public Flux<ProductItem> getProductItems();

    Mono<ProductItem> saveProductItem(final ProductItem productItem);
}
