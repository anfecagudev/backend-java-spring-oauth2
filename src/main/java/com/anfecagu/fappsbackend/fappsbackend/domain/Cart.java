package com.anfecagu.fappsbackend.fappsbackend.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Cart {
    private String id;
    private long userId;
    private List<CartItem> products;
}
