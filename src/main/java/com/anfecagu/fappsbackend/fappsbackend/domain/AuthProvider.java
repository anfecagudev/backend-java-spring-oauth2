package com.anfecagu.fappsbackend.fappsbackend.domain;

public enum  AuthProvider {
    local,
    google
}
