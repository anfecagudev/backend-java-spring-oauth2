package com.anfecagu.fappsbackend.fappsbackend.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CartItem {

    private double quantity;
    private ProductItem productItem;

}
