package com.anfecagu.fappsbackend.fappsbackend.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductItem {
    private String id;
    private String imageUrl;
    private String type;
    private String code;
    private String unit;
    private double price;
    private String description;
}
