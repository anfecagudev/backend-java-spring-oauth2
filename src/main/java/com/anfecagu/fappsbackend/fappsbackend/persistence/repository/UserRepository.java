package com.anfecagu.fappsbackend.fappsbackend.persistence.repository;

import com.anfecagu.fappsbackend.fappsbackend.domain.User;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface UserRepository extends ReactiveMongoRepository<User, Long> {

    Mono<User> findByEmail(String email);

    Boolean existsByEmail(String email);

}
