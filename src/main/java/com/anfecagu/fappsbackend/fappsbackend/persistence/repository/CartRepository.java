package com.anfecagu.fappsbackend.fappsbackend.persistence.repository;


import com.anfecagu.fappsbackend.fappsbackend.persistence.CartDTO;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface CartRepository extends ReactiveMongoRepository<CartDTO, Long> {

    Mono<CartDTO> findByUserId(final Long userId);
}
