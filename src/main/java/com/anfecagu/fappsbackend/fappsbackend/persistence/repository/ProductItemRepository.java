package com.anfecagu.fappsbackend.fappsbackend.persistence.repository;

import com.anfecagu.fappsbackend.fappsbackend.persistence.ProductItemDTO;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductItemRepository extends ReactiveMongoRepository<ProductItemDTO, String> {
}
