package com.anfecagu.fappsbackend.fappsbackend.persistence;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;


@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductItemDTO {
        private String id;
        private String imageUrl;
        private String type;
        private String code;
        private String unit;
        private double price;
        private String description;
}
