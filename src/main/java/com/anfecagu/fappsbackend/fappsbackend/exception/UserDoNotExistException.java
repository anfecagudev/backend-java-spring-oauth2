package com.anfecagu.fappsbackend.fappsbackend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserDoNotExistException extends AuthenticationException {
    public UserDoNotExistException(String msg, Throwable t) {
        super(msg, t);
    }

    public UserDoNotExistException(String msg) {
        super(msg);
    }
}
